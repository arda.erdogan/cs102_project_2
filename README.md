<h2>Mine Sweeper Game</h2>
<h5>Add the following features.</h5>

<ul>
  <li>Obtain the same look and feel as the screenshot below. On the upper left there is a counter
which shows the (total number of mines minus flagged ones). On the right there is a timer.</li>
  <li>When the user makes a right-click on a button, that cell shall be “flagged”. Display a flag icon
on the button.</li>
  <li>If all the mines are flagged, pop up a message saying “You’re a genius.” and end the game.</li>
  <li>If the user right clicks on a flagged cell, the flag should be removed. Once a button is clicked, ‐
clicking it again should have no effect.</li>
  <li>. If the user clicks on a mine show all the mines and change the face icon from happy to sad</li>
  <li>When the user opens a “0” cell (i.e. a cell with no mines on surrounding neighbours) open up
all the neighbours as well and keep doing the same thing recursively if the neighbours are “0”
as well.</li>
</ul>